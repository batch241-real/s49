// Mock database

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => showPosts(data));


// ADD Post data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	// prevents the default behavior of an event
	// prevent the page from reloading (default of submit)
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
		headers: {'Content-type':'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Post successfully added!');
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	});
});

// RETRIEVE post
const showPosts = (posts) => {
	// variable that will contain all the posts
	let postEntries = '';
	// loop through array items
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}"">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});
	document.querySelector('#div-post-entries').innerHTML = postEntries;
};


// EDIT post
const editPost = (id) => {

	// function first uses the querySelector() method to get the element with the id '#post-title-${id}' and '#post-body-${id}' and assigns its innerHTML property to the title variable with the same body

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	// remove Attribute() - removes the attribute with the specified name from the element
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
};


// UPDATE post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PATCH',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {'Content-type':'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Post successfully updated!');
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		document.querySelector('#btn-submit-update').setAttribute('disabled', true)
	});
});


// ACTIVTY DELETE post
const deletePost = (id) => {
	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'DELETE',
		headers: {'Content-type':'application/json; charset=UTF-8'} 
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data)
		document.querySelector(`#post-${id}`).remove()
	});
	alert("Post deleted successfully!")
}